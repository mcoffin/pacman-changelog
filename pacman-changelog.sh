#!/usr/bin/zsh
#    pacman-changelog.sh filters your pacman log file for installs, updates, and reinstalls
#    Copyright (C) Matt Coffin <mcoffin13@gmail.com>
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.

set -e
set -o pipefail

handle_opt_unknowns() {
	if [ $# -lt 2 ]; then
		echo 'Usage: handle_opt_unknowns OPT OPTARG' >&2
		# We want to actually exit since the return val is used to hint at other stuff
		exit 1
	fi
	local opt="$1"
	local OPTARG="$2"
	case ${opt} in
		\?)
			echo "Unknown argument: -$OPTARG" >&2
			return 1
			;;
		:)
			printf 'Invalid argument: -%s requires an argument\n' "$OPTARG" >&2
			return 1
			;;
	esac
	return 0
}

log_files=()
verbosity=0
dry_run=0
colorize=0

while getopts ":udf:vcy" opt; do
	case ${opt} in
		u)
			pattern_prefix="(up)"
			;;
		d)
			pattern_prefix="(down)"
			;;
		f)
			log_files+=("$OPTARG")
			;;
		v)
			verbosity=$((verbosity + 1))
			;;
		y)
			dry_run=1
			;;
		c)
			colorize=1
			;;
		*)
			set +e
			handle_opt_unknowns "$opt" "$OPTARG"
			[ $? -eq 0 ] || exit $?
			set -e
			;;
	esac
done
shift $((OPTIND -1))

[ $verbosity -lt 3 ] || set -x

if [ ${#log_files[@]} -lt 1 ]; then
	log_files=(/var/log/pacman.log)
fi

print_file_header() {
	local OPTIND
	local opt
	local eflags="-e"
	while getopts ":n" opt; do
		case ${opt} in
			n)
				eflags="$eflags -n"
				;;
			*)
				set +e
				handle_opt_unknowns "$opt" "$OPTARG"
				[ $? -eq 0 ] || exit $?
				set -e
				;;
		esac
	done
	shift $((OPTIND -1))

	echo $eflags "\033[1;36m$1\033[0m:"
}

log_debug() {
	local level=1
	if [ $(echo "$1" | grep -E '^-?[0-9]+$' | wc -l) -ge 1 ]; then
		level=$1
		shift
	fi
	if [ -z "$DEBUG" -a $verbosity -lt $level ]; then
		return 0
	fi
	if [ $# -gt 0 ]; then
		echo "$@"
	else
		tee /dev/null
	fi
}

e_cond() {
	if [ $# -lt 1 ]; then
		echo 'Usage: e_cond EXECUTABLE [ARGS...]' >&2
		return 1
	fi
	local executable="$1"
	shift
	[ -x "$(command -v "$executable")" ] || return 0
	"$executable" "$@"
}

colorize_output() {
	if [ $colorize -eq 0 ]; then
		tee /dev/null
	else
		local cyan=$(printf '\033[1;36m')
		local green=$(printf '\033[0;32m')
		local yellow=$(printf '\033[1;33m')
		local red=$(printf '\033[0;31m')
		local blue=$(printf '\033[0;34m')
		local reset=$(printf '\033[0m')
		sed -E 's/^\[.*\] *//g' \
			| sed -E "s/^([a-zA-Z]+) +([a-zA-Z0-9_\\-]+)/\\1 $cyan\\2$reset/g" \
			| sed -E "s/^(installed)/$green\\1$reset/g" \
			| sed -E "s/^(reinstalled)/$yellow\\1$reset/g" \
			| sed -E "s/^(downgrad((ed)|(ing)))/$red\\1$reset/g" \
			| sed -E "s/^(upgrad((ed)|(ing)))/$blue\\1$reset/g"
	fi
}

cleanup_output() {
	awk '{ if (length($0) > 0) { print $0; } }' \
		| sed -E 's/ \[ALPM\] / /g'
}

alias _dumpargs='e_cond dumpargs'

[ $verbosity -le 0 ] || log_debug "$(printf "verbosity: %d" $verbosity)"

for log_file in ${log_files[@]}; do
	if [ ${#log_files[@]} -gt 1 ]; then
		print_file_header "$log_file"
	fi
	pattern='\[ALPM\] +((((up)|(down))grad((ed)|(ing))?)|((re)?install((ing)|(ed))?))'
	packages=("$@")
	if [ ${#packages[@]} -gt 0 ]; then
		pattern="$pattern ("
		package_idx=0
		for package in ${packages[@]}; do
			if [ $package_idx -gt 0 ]; then
				pattern="$pattern|"
			fi
			pattern="$pattern($package)"
			package_idx=$((package_idx + 1))
		done
		pattern="$pattern) "
	fi
	printf 'pattern: "%s"\n' "$pattern" | log_debug >&2
	[ ! -x "$(command -v dumpargs)" ] || log_debug 2 -n "args: " >&2
	_dumpargs -E "$pattern" | (tr '\n' ' '; [ ! -x "$(command -v dumpargs)" ] || echo) | log_debug 2 >&2
	[ $dry_run -eq 0 ] || exit 0
	grep -E "$pattern" "$log_file" | cleanup_output | colorize_output
done
